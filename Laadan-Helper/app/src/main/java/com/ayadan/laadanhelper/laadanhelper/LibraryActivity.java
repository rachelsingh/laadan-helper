package com.ayadan.laadanhelper.laadanhelper;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class LibraryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void gotoUrl(String url)
    {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void gotoAyaDanYouTube(View view)
    {
        gotoUrl("https://www.youtube.com/ayadanconlangs");
    }
    public void gotoRuthBlog(View view)
    {
        gotoUrl("http://laadan.tumblr.com/");
    }
    public void gotoLaadanClub(View view)
    {
        gotoUrl("http://laadan.club/");
    }

    public void loadLesson( View v, String url )
    {
        WebView mWebView = (WebView) findViewById(R.id.webView);
        mWebView.loadUrl(url);

        ScrollView lessonList = (ScrollView) findViewById( R.id.TableOfContents);
        LinearLayout lessonReader = (LinearLayout) findViewById( R.id.ReadStory);

        lessonList.setVisibility(View.GONE);
        lessonReader.setVisibility(View.VISIBLE);

        ScrollView scrollLesson = (ScrollView) findViewById(R.id.scrollLesson);
        scrollLesson.setScrollY(0);
    }

    public void viewAll( View v )
    {

        ScrollView lessonList = (ScrollView) findViewById( R.id.TableOfContents);
        LinearLayout lessonReader = (LinearLayout) findViewById( R.id.ReadStory);

        lessonList.setVisibility(View.VISIBLE);
        lessonReader.setVisibility(View.GONE);
    }

    public void loadStory_Suzette1( View v )
    {
        loadLesson(v, "file:///android_asset/suzette-1.html");
    }

    public void loadStory_Suzette2( View v )
    {
        loadLesson(v, "file:///android_asset/suzette-2.html");
    }

    public void loadStory_Suzette4( View v )
    {
        loadLesson(v, "file:///android_asset/suzette-4.html");
    }

    public void loadStory_Suzette5( View v )
    {
        loadLesson(v, "file:///android_asset/suzette-5.html");
    }

    public void loadStory_Suzette6( View v )
    {
        loadLesson(v, "file:///android_asset/suzette-6.html");
    }

    public void loadStory_Amberwind1( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-1.html");
    }

    public void loadStory_Amberwind2( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-2.html");
    }

    public void loadStory_Amberwind3( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-3.html");
    }

    public void loadStory_Amberwind4( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-4.html");
    }

    public void loadStory_Amberwind5( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-5.html");
    }

    public void loadStory_Amberwind6( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-6.html");
    }

    public void loadStory_Amberwind7( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-7.html");
    }

    public void loadStory_Amberwind8( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-8.html");
    }

    public void loadStory_Amberwind9( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-9.html");
    }

    public void loadStory_Amberwind10( View v )
    {
        loadLesson(v, "file:///android_asset/amberwind-10.html");
    }

    public void loadStory_Aesop1( View v )
    {
        loadLesson(v, "file:///android_asset/aesop-1.html");
    }

    public void loadStory_Aesop2( View v )
    {
        loadLesson(v, "file:///android_asset/aesop-2.html");
    }

    public void loadStory_Aesop3( View v )
    {
        loadLesson(v, "file:///android_asset/aesop-3.html");
    }

    public void loadStory_Aesop4( View v )
    {
        loadLesson(v, "file:///android_asset/aesop-4.html");
    }
}
