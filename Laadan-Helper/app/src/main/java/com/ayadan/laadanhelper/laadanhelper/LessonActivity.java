package com.ayadan.laadanhelper.laadanhelper;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class LessonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void gotoMoreResources(View view)
    {
        Intent intent = new Intent(this, ResourceLinksActivity.class);
        startActivity(intent);
    }

    public void loadLesson( View v, String url )
    {
        WebView mWebView = (WebView) findViewById(R.id.webView);
        mWebView.loadUrl(url);

        LinearLayout lessonList = (LinearLayout) findViewById( R.id.TableOfContents);
        LinearLayout lessonReader = (LinearLayout) findViewById( R.id.ReadLesson);

        lessonList.setVisibility(View.GONE);
        lessonReader.setVisibility(View.VISIBLE);

        ScrollView scrollLesson = (ScrollView) findViewById(R.id.scrollLesson);
        scrollLesson.setScrollY(0);
    }

    public void viewAll( View v )
    {

        LinearLayout lessonList = (LinearLayout) findViewById( R.id.TableOfContents);
        LinearLayout lessonReader = (LinearLayout) findViewById( R.id.ReadLesson);

        lessonList.setVisibility(View.VISIBLE);
        lessonReader.setVisibility(View.GONE);
    }

    public void loadLesson11( View v )
    {
        loadLesson(v, "file:///android_asset/lesson1-1.html");
    }

    public void loadLesson12( View v )
    {
        loadLesson(v, "file:///android_asset/lesson1-2.html");
    }

    public void loadLesson13( View v )
    {
        loadLesson(v, "file:///android_asset/lesson1-3.html");
    }

    public void loadLesson14( View v )
    {
        loadLesson(v, "file:///android_asset/lesson1-4.html");
    }

    public void loadLesson15( View v )
    {
        loadLesson(v, "file:///android_asset/lesson1-5.html");
    }

    public void loadLesson21( View v )
    {
        loadLesson(v, "file:///android_asset/lesson2-1.html");
    }

    public void loadLesson22( View v )
    {
        loadLesson(v, "file:///android_asset/lesson2-2.html");
    }

    public void loadLesson23( View v )
    {
        loadLesson(v, "file:///android_asset/lesson2-3.html");
    }

    public void loadLesson24( View v )
    {
        loadLesson(v, "file:///android_asset/lesson2-4.html");
    }

    public void loadLesson25( View v )
    {
        loadLesson(v, "file:///android_asset/lesson2-5.html");
    }

    public void loadLesson31( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-1.html");
    }

    public void loadLesson32( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-2.html");
    }

    public void loadLesson33( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-3.html");
    }

    public void loadLesson34( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-4.html");
    }

    public void loadLesson35( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-5.html");
    }

    public void loadLesson36( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-6.html");
    }

    public void loadLesson37( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-7.html");
    }

    public void loadLesson38( View v )
    {
        loadLesson(v, "file:///android_asset/lesson3-8.html");
    }

    public void loadLesson41( View v )
    {
        loadLesson(v, "file:///android_asset/lesson4-1.html");
    }

    public void loadLesson42( View v )
    {
        loadLesson(v, "file:///android_asset/lesson4-2.html");
    }

    public void loadLesson43( View v )
    {
        loadLesson(v, "file:///android_asset/lesson4-3.html");
    }

}

