package com.ayadan.laadanhelper.laadanhelper;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class SocialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void gotoUrl(String url)
    {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void gotoDiscord(View view)
    {
        gotoUrl("https://discord.gg/HUzh5EF");
    }

    public void gotoLaadanClub(View view)
    {
        gotoUrl("http://laadan.club");
    }

    public void gotoFacebookGroup(View view)
    {
        gotoUrl("https://www.facebook.com/groups/1510434509169558/");
    }

    public void gotoLivejournal(View view)
    {
        gotoUrl("http://laadan.livejournal.com/");
    }

    public void gotoAyaDan(View view)
    {
        gotoUrl("http://ayadan.moosader.com/category/language/laadan/");
    }

    public void gotoThamezhinBlog(View view)
    {
        gotoUrl("http://laadan.tumblr.com/");
    }

    public void gotoSuzetteBlog(View view)
    {
        gotoUrl("https://ozarque.livejournal.com/");
    }
}
