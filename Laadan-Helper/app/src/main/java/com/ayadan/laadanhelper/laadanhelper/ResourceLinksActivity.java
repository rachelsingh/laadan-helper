package com.ayadan.laadanhelper.laadanhelper;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class ResourceLinksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_links);
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, LessonActivity.class);
        startActivity(intent);
    }

    public void gotoUrl(String url)
    {
        Uri uriUrl = Uri.parse(url);
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    public void gotoOfficial(View view)
    {
        gotoUrl("https://laadanlanguage.wordpress.com/");
    }

    public void gotoAmberwind(View view)
    {
        gotoUrl("http://ayadan.moosader.com/archive/LaadanLessons/");
    }

    public void gotoWikibook(View view)
    {
        gotoUrl("https://en.wikibooks.org/wiki/L%C3%A1adan");
    }

    public void gotoDictionary(View view)
    {
        gotoUrl("http://ayadan.moosader.com/archive/LaadanBook_NoDictionary.pdf");
    }
}
