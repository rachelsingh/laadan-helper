package com.ayadan.laadanhelper.laadanhelper;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DictionaryActivity extends AppCompatActivity {

    List<String> dictionary = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        loadDictionary();
    }

    public void goBack(View view)
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void searchDictionary(View view)
    {
        EditText searchField = (EditText) findViewById(R.id.txtSearch);
        search(view, searchField.getText().toString());
    }

    public void loadDictionary()
    {
        Resources res = getResources();

        InputStream inStream = res.openRawResource(R.raw.laadan_to_englishb);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

        dictionary.clear();

        String results = "";

        try {
            String line = reader.readLine();
            line = "bob";
            while (line != null) {
                line = reader.readLine();

                if (line != null ) {
                    dictionary.add(line);
                }
            }
            results = "";
        } catch (IOException e) {
            e.printStackTrace();
            results = "Error";
        }

        TextView text = (TextView) findViewById(R.id.txtResults);
        text.setText(results);
    }

    public void search( View v, String searchTerm ) {

        String results = "";

        TextView text = (TextView) findViewById(R.id.txtResults);

        String search = searchTerm.toLowerCase();

        for ( int i = 0; i < dictionary.size(); i++ )
        {
            // á é í ó ú
            // ignore case and accents
            String line = dictionary.get(i).toLowerCase();
            line = line.replace("á","a");
            line = line.replace("é","e");
            line = line.replace("í","i");
            line = line.replace("ó","o");
            line = line.replace("ú","u");

            String userLine = dictionary.get(i).replace("~","\n► ");

            if ( line == "" || line == null )
            {
                continue;
            }

            Boolean match;

            try {
                match = line.contains(search);
            }
            catch( Exception ex )
            {
                results += ex.toString() + System.getProperty("line.separator");
                results += "Length: " + line.length() + System.getProperty("line.separator");
                results += "Text: \"" + line + "\"" + System.getProperty("line.separator");
                results += "Search: \"" + searchTerm + "\"" + System.getProperty("line.separator");
                text.setText(results);
                return;
            }

            if (match)
            {
                results += userLine + System.getProperty("line.separator") + System.getProperty("line.separator");
            }
        }

        text.setText(results);

    }

}
